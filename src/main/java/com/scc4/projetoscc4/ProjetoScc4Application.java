package com.scc4.projetoscc4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoScc4Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoScc4Application.class, args);
	}

}
