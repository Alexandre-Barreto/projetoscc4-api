package com.scc4.projetoscc4.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scc4.projetoscc4.domain.Remetentes;

@Repository
public interface RemetentesRepository extends JpaRepository<Remetentes,Integer>{
	
}
