package com.scc4.projetoscc4.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;

@Entity
public class Remetentes{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRemetentes")
	private Integer idRemetente;
	
	@Column(name = "nome", length = 70)
	private String nome;
	
	@Column(name = "telefone", length = 12)
	private String telefone;
	
	@Column(name = "cpf", length = 11)
	private String cpf;
	
	@Column(name = "endereco", length = 70)
	private String endereco;
	
	@Column(name = "email", length = 50)
	@Email
	private String email;

	public Integer getIdRemetente() {
		return idRemetente;
	}

	public void setIdRemetente(Integer id) {
		this.idRemetente = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
