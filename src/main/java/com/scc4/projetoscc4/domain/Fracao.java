package com.scc4.projetoscc4.domain;

public class Fracao {
	
	private Integer numerador;
	private Integer denominador;
	private Integer segundoNumerador;
	private Integer segundoDenominador;
	private Integer resultadoNumerador;
	private Integer resultadoDenominador;

	public Integer getResultadoNumerador() {
		return resultadoNumerador;
	}
	public void setResultadoNumerador(Integer numeradorResultado) {
		this.resultadoNumerador = numeradorResultado;
	}
	public Integer getResultadoDenominador() {
		return resultadoDenominador;
	}
	public void setResultadoDenominador(Integer denominadorResultado) {
		this.resultadoDenominador = denominadorResultado;
	}
	public Integer getSegundoDenominador() {
		return segundoDenominador;
	}
	public void setSegundoDenominador(Integer segundoDenominador) {
		this.segundoDenominador = segundoDenominador;
	}
	public Integer getSegundoNumerador() {
		return segundoNumerador;
	}
	public void setSegundoNumerador(Integer segundoNumerador) {
		this.segundoNumerador = segundoNumerador;
	}
	public Integer getDenominador() {
		return denominador;
	}
	public void setDenominador(Integer denominador) {
		this.denominador = denominador;
	}
	public Integer getNumerador() {
		return numerador;
	}
	public void setNumerador(Integer numerador) {
		this.numerador = numerador;
	}

}
