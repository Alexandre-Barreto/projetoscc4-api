package com.scc4.projetoscc4.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scc4.projetoscc4.domain.Fracao;
import com.scc4.projetoscc4.domain.Remetentes;
import com.scc4.projetoscc4.service.CadastroRemetentesService;
import com.scc4.projetoscc4.service.CalculadoraFracaoService;
import com.scc4.projetoscc4.service.ProjetoScc4Service;

@RestController
public class ProjetoScc4Controller {

	@Autowired
	private ProjetoScc4Service service;
	
	@Autowired
	private CalculadoraFracaoService calculadoraService;
	
	@Autowired
	private CadastroRemetentesService cadastroRemetentesService;

	@GetMapping("/listaReversa")
	public ArrayList<String> listaReversa(@RequestParam String lista) {
		return service.listaReversa(lista);
	}

	@GetMapping("/imprimirImpares")
	public ArrayList<Integer> imprimirImpares(@RequestParam String lista) {
		return service.imprimirImpares(lista);
	}

	@GetMapping("/imprimirPares")
	public ArrayList<Integer> imprimirPares(@RequestParam String lista) {
		return service.imprimirPares(lista);
	}

	@GetMapping("/tamanho")
	public String tamanhoPalavra(@RequestParam String palavra) {
		return service.tamanhoPalavra(palavra);
	}

	@GetMapping("/maisculas")
	public String letrasMaisculas(@RequestParam String palavra) {
		return service.letrasMaisculas(palavra);
	}

	@GetMapping("/vogais")
	public String vogais(@RequestParam String palavra) {
		return service.vogais(palavra);
	}

	@GetMapping("/consoantes")
	public String consoantes(@RequestParam String palavra) {
		return service.consoantes(palavra);
	}

	@GetMapping("/nomeBibliografico")
	public String nomeBibliografico(@RequestParam String nome) {
		return service.nomeBibliografico(nome);
	}

	// Sistema Monetario - EX: GET
	// http://localhost:8080/sistemaMonetario/opcaoSaque?valorSaque=8
	@GetMapping("/sistemaMonetario/opcaoSaque")
	public String sistemaMonetario(@RequestParam String valorSaque) {
		return service.sistemaMonetario(valorSaque);
	}

	/* Calculadora de Fração
	 * Exemplo de corpo da requisição
	 * {	
		"numerador": 2,
		"denominador": 15,
		"segundoNumerador": 5,
		"segundoDenominador": 8
		}
		
		RETORNO EX :
		{
		    "numerador": 2,
		    "denominador": 15,
		    "segundoNumerador": 5,
		    "segundoDenominador": 8,
		    "resultadoNumerador": 91,
		    "resultadoDenominador": 120
		}	
	*/	
	
	/*|SOMA|
	 * POST http://localhost:8080/calculadora/soma
	 */
	
	@PostMapping("calculadora/soma")
	public Fracao soma(@RequestBody Fracao fracao) {
		return calculadoraService.soma(fracao);		
	}
	
	/*|SUBTRAÇÃO|
	 * POST http://localhost:8080/calculadora/subtracao
	 */
	
	@PostMapping("calculadora/subtracao")
	public Fracao subtracao(@RequestBody Fracao fracao) {
		return calculadoraService.subtracao(fracao);		
	}
	
	/*|MULTIPLICAÇÃO||
	 * POST http://localhost:8080/calculadora/multiplicacao
	 */
	
	@PostMapping("calculadora/multiplicacao")
	public Fracao multiplicacao(@RequestBody Fracao fracao) {
		return calculadoraService.multiplicacao(fracao);		
	}
	
	/*|DIVISÃO||
	 * POST http://localhost:8080/calculadora/divisao
	 */
	
	@PostMapping("calculadora/divisao")
	public Fracao divisao(@RequestBody Fracao fracao) {
		return calculadoraService.divisao(fracao);		
	}
	//
	
	//Cadastro de Remetentes//
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("remetentes/save")
	public Remetentes save(@RequestBody Remetentes remetentes) {
		return cadastroRemetentesService.saveRemetentes(remetentes);		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("remetentes/list")
	public List<Remetentes> list(){		
		return cadastroRemetentesService.list();
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("remetentes/list/{id}")
	public Optional<Remetentes> listById(@PathVariable Integer id){
		return cadastroRemetentesService.listById(id);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("remetentes/update/{id}")
	public void update(@RequestBody Remetentes remetentes,@PathVariable Integer id){		
		cadastroRemetentesService.atualizarLista(remetentes, id);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("remetentes/delete/{id}")
	public void delete(@PathVariable Integer id){		
		cadastroRemetentesService.deleteById(id);
	}
	
}
