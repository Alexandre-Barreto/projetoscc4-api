package com.scc4.projetoscc4.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ProjetoScc4Service {

	public ArrayList<String> listaReversa(String lista) {
		String[] listaSeparada = lista.split(",");
		ArrayList<String> listaResposta = new ArrayList<>(listaSeparada.length);
		for (int a = 0; a >= listaResposta.size(); a++) {
			for (int i = listaSeparada.length - 1; i >= 0; i--) {
				listaResposta.add(listaSeparada[i]);
			}
		}
		return listaResposta;
	}

	public ArrayList<Integer> imprimirImpares(String lista) {
		lista = lista.replace("{", "").replace("}", "");
		String[] listaSeparada = lista.split(",");
		ArrayList<Integer> listaImpares = new ArrayList<>(listaSeparada.length);
		for (int i = 0; i <= listaSeparada.length - 1; i++) {
			int listaNumeros = Integer.parseInt(listaSeparada[i]);
			if (listaNumeros % 2 != 0) {
				listaImpares.add(listaNumeros);
			}
		}
		return listaImpares;
	}

	public ArrayList<Integer> imprimirPares(String lista) {
		lista = lista.replace("{", "").replace("}", "");
		String[] listaSeparada = lista.split(",");
		ArrayList<Integer> listaPares = new ArrayList<>(listaSeparada.length);
		for (int i = 0; i <= listaSeparada.length - 1; i++) {
			int listaNumeros = Integer.parseInt(listaSeparada[i]);
			if (listaNumeros % 2 == 0) {
				listaPares.add(listaNumeros);
			}
		}
		return listaPares;
	}

	public String tamanhoPalavra(String palavra) {
		return "tamanho : " + palavra.length();

	}

	public String letrasMaisculas(String palavra) {
		return palavra.toUpperCase();

	}

	public String vogais(String palavra) {
		char[] letras = palavra.toCharArray();
		char[] vogais = new char[5];
		vogais[0] = 'a';
		vogais[1] = 'e';
		vogais[2] = 'i';
		vogais[3] = 'o';
		vogais[4] = 'u';
		String listaVogais = "";

		for (int i = 0; i <= 4; i++) {
			for (int y = 0; y <= letras.length - 1; y++) {
				if (vogais[i] == letras[y]) {
					listaVogais += vogais[i];
				}
			}
		}
		return listaVogais;
	}

	public String consoantes(String palavra) {
		char[] letras = palavra.toCharArray();
		char[] consoantes = new char[19];
		consoantes[0] = 'b';
		consoantes[1] = 'c';
		consoantes[2] = 'd';
		consoantes[3] = 'f';
		consoantes[4] = 'g';
		consoantes[5] = 'j';
		consoantes[6] = 'k';
		consoantes[7] = 'l';
		consoantes[8] = 'm';
		consoantes[9] = 'n';
		consoantes[10] = 'p';
		consoantes[11] = 'q';
		consoantes[12] = 'r';
		consoantes[13] = 's';
		consoantes[14] = 't';
		consoantes[15] = 'v';
		consoantes[16] = 'x';
		consoantes[17] = 'y';
		consoantes[18] = 'z';

		String listaConsoantes = "";

		for (int i = 0; i <= 18; i++) {
			for (int y = 0; y <= letras.length - 1; y++) {
				if (consoantes[i] == letras[y]) {
					listaConsoantes += letras[y];
				}
			}
		}
		return listaConsoantes;
	}

	public String nomeBibliografico(String nome) {
		String ultimoNome = nome.substring(nome.lastIndexOf("%")).replace("%", "");
		nome = nome.replace(ultimoNome, "");
		ultimoNome = ultimoNome.toUpperCase();

		String[] nomeSeparado = nome.split("%");
		StringBuilder sb = new StringBuilder();
		sb.append(ultimoNome).append(", ");
		for (int a = 0; a <= nomeSeparado.length - 1; a++) {
			sb.append(nomeSeparado[a].substring(0, 1).toUpperCase()).append(nomeSeparado[a].substring(1).toLowerCase())
					.append(" ");
		}
		String nomeBibliografico = sb.toString();
		return nomeBibliografico;
	}

	public String sistemaMonetario(String valorSaque) {
		Long valor = null;
		int notasCinco = 0;
		int notasTres = 0;
		String resultado = "";
		try {
			valor = Long.parseLong(valorSaque);
		} catch (Exception e) {
		}
		if (valor < 8) {
			return "O valor minimo de saque é R$8";
		}

		while (valor >= 5) {
			notasCinco++;
			valor -= 5;
		}
		while (valor >= 3) {
			notasTres++;
			valor -= 3;
		}
		if (notasTres > 0) {
			if (notasTres > 1) {
				resultado = "Saque R$" + valorSaque + ": " + notasTres + " notas de R$3 ";
			}			
			resultado = "Saque R$" + valorSaque + ": " + notasTres + " nota de R$3 ";
			if(notasCinco > 1) {
				resultado = resultado + " e " + notasCinco + " notas de R$5";
			}
			resultado = resultado + " e " + notasCinco + " nota de R$5";
		} else {
			resultado = "Saque R$" + valorSaque + ": " + notasCinco + " notas de R$5 ";
		}
		return resultado;
	}

}
