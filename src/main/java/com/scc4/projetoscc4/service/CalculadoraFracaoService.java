package com.scc4.projetoscc4.service;

import org.springframework.stereotype.Service;

import com.scc4.projetoscc4.domain.Fracao;

@Service
public class CalculadoraFracaoService {

	public Fracao soma(Fracao fracao) {
		Integer numerador = fracao.getNumerador();
		Integer denominador = fracao.getDenominador();
		Integer segundoDenominador = fracao.getSegundoDenominador();
		Integer segundoNumerador = fracao.getSegundoNumerador();
		// Simplificação
		int[] resultadoSimplificado = this.simplificacao(numerador, denominador);
		numerador = resultadoSimplificado[0];
		denominador = resultadoSimplificado[1];

		resultadoSimplificado = this.simplificacao(segundoNumerador, segundoDenominador);
		segundoNumerador = resultadoSimplificado[0];
		segundoDenominador = resultadoSimplificado[1];
		//
		// Verifica se os denominadores são iguais
		if (denominador == segundoDenominador) {
			numerador += segundoNumerador;
			int[] resultadoFinal = this.simplificacao(numerador, denominador);
			numerador = resultadoFinal[0];
			denominador = resultadoFinal[1];
			fracao.setResultadoDenominador(denominador);
			fracao.setResultadoNumerador(numerador);
			return fracao;
		}
		//
		int resultadoMMC = this.mmc(denominador, segundoDenominador);
		numerador *= segundoDenominador;
		segundoNumerador *= denominador;
		numerador += segundoNumerador;
		int[] resultadoFinal = this.simplificacao(numerador, resultadoMMC);
		numerador = resultadoFinal[0];
		denominador = resultadoFinal[1];
		fracao.setResultadoNumerador(numerador);
		fracao.setResultadoDenominador(denominador);
		return fracao;
	}

	public Fracao subtracao(Fracao fracao) {
		Integer numerador = fracao.getNumerador();
		Integer denominador = fracao.getDenominador();
		Integer segundoDenominador = fracao.getSegundoDenominador();
		Integer segundoNumerador = fracao.getSegundoNumerador();
		// Simplificação
		int[] resultadoSimplificado = this.simplificacao(numerador, denominador);
		numerador = resultadoSimplificado[0];
		denominador = resultadoSimplificado[1];

		resultadoSimplificado = this.simplificacao(segundoNumerador, segundoDenominador);
		segundoNumerador = resultadoSimplificado[0];
		segundoDenominador = resultadoSimplificado[1];
		//
		// Verifica se os denominadores são iguais
		if (denominador == segundoDenominador) {
			numerador -= segundoNumerador;
			int[] resultadoFinal = this.simplificacao(numerador, denominador);
			numerador = resultadoFinal[0];
			denominador = resultadoFinal[1];
			fracao.setResultadoDenominador(denominador);
			fracao.setResultadoNumerador(numerador);
			return fracao;
		}
		//
		int resultadoMMC = this.mmc(denominador, segundoDenominador);
		numerador *= segundoDenominador;
		segundoNumerador *= denominador;
		numerador -= segundoNumerador;
		int[] resultadoFinal = this.simplificacao(numerador, resultadoMMC);
		numerador = resultadoFinal[0];
		denominador = resultadoFinal[1];
		fracao.setResultadoNumerador(numerador);
		fracao.setResultadoDenominador(denominador);
		return fracao;
	}

	public Fracao multiplicacao(Fracao fracao) {
		Integer numerador = fracao.getNumerador();
		Integer denominador = fracao.getDenominador();
		Integer segundoDenominador = fracao.getSegundoDenominador();
		Integer segundoNumerador = fracao.getSegundoNumerador();
		// Simplificação
		int[] resultadoSimplificado = this.simplificacao(numerador, denominador);
		numerador = resultadoSimplificado[0];
		denominador = resultadoSimplificado[1];

		int[] simplificacaoCruzada = this.simplificacao(numerador, segundoDenominador);
		numerador = simplificacaoCruzada[0];
		segundoDenominador = simplificacaoCruzada[1];

		simplificacaoCruzada = this.simplificacao(segundoNumerador, denominador);
		segundoNumerador = simplificacaoCruzada[0];
		denominador = simplificacaoCruzada[1];

		if (simplificacaoCruzada.equals(null)) {
			resultadoSimplificado = this.simplificacao(segundoNumerador, segundoDenominador);
			segundoNumerador = resultadoSimplificado[0];
			segundoDenominador = resultadoSimplificado[1];
		}

		numerador *= segundoNumerador;
		denominador *= segundoDenominador;
		int[] resultadoFinal = this.simplificacao(numerador, denominador);
		numerador = resultadoFinal[0];
		denominador = resultadoFinal[1];
		fracao.setResultadoNumerador(numerador);
		fracao.setResultadoDenominador(denominador);

		return fracao;
	}

	public Fracao divisao(Fracao fracao) {
		Integer numerador = fracao.getNumerador();
		Integer denominador = fracao.getDenominador();
		Integer segundoDenominador = fracao.getSegundoNumerador();
		Integer segundoNumerador = fracao.getSegundoDenominador();
		// Simplificação

		int[] resultadoSimplificado = this.simplificacao(numerador, denominador);
		numerador = resultadoSimplificado[0];
		denominador = resultadoSimplificado[1];

		int[] simplificacaoCruzada = this.simplificacao(numerador, segundoDenominador);
		numerador = simplificacaoCruzada[0];
		segundoDenominador = simplificacaoCruzada[1];

		simplificacaoCruzada = this.simplificacao(segundoNumerador, denominador);
		segundoNumerador = simplificacaoCruzada[0];
		denominador = simplificacaoCruzada[1];

		if (simplificacaoCruzada.equals(null)) {
			resultadoSimplificado = this.simplificacao(segundoNumerador, segundoDenominador);
			segundoNumerador = resultadoSimplificado[0];
			segundoDenominador = resultadoSimplificado[1];
		}

		numerador *= segundoNumerador;
		denominador *= segundoDenominador;
		int[] resultadoFinal = this.simplificacao(numerador, denominador);
		numerador = resultadoFinal[0];
		denominador = resultadoFinal[1];
		fracao.setResultadoNumerador(numerador);
		fracao.setResultadoDenominador(denominador);

		return fracao;
	}

	private Integer maximoDivisor(Integer primeiroNumero, Integer segundoNumero) {
		while (segundoNumero != 0) {
			int resultado = primeiroNumero % segundoNumero;
			primeiroNumero = segundoNumero;
			segundoNumero = resultado;
		}
		return primeiroNumero;
	}

	// MMC
	private Integer mmc(Integer denominador, Integer segundoDenominador) {
		return denominador * (segundoDenominador / maximoDivisor(denominador, segundoDenominador));
	}

	private int[] simplificacao(Integer numerador, Integer denominador) {
		int divisor = this.maximoDivisor(numerador, denominador);

		if (divisor <= numerador && divisor <= denominador) {
			numerador /= divisor;
			denominador /= divisor;
		}
		int[] resultadoSimplificado = new int[2];
		resultadoSimplificado[0] = numerador;
		resultadoSimplificado[1] = denominador;
		return resultadoSimplificado;
	}

}
