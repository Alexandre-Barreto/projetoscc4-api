package com.scc4.projetoscc4.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.scc4.projetoscc4.domain.Remetentes;
import com.scc4.projetoscc4.repository.RemetentesRepository;

@Service
public class CadastroRemetentesService {
	
	@Autowired
	RemetentesRepository repository ;
	
	public Remetentes saveRemetentes(Remetentes remetentes) {
		return repository.save(remetentes);
	}

	public List<Remetentes> list() {
		return repository.findAll();
	}
	
	public Optional<Remetentes> listById(	Integer id) {
		return repository.findById(id);
	}
	
	public Optional<Remetentes> atualizarLista(Remetentes remetentes,Integer id) {
		Optional<Remetentes> remetentesByID = this.listById(id);
		if(remetentesByID.isEmpty()) {
			return remetentesByID;
		}
		this.deleteById(id);
		this.saveRemetentes(remetentes);
		return remetentesByID;
	}
	
	public void deleteById(Integer id) {
		repository.deleteById(id);
	}
	
}
